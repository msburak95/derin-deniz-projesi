﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DerinDenizProjesi.Models;

namespace DerinDenizProjesi.Controllers
{
    public class KullaniciController : Controller
    {
        private UyeDBEntities3 db = new UyeDBEntities3();

        // GET: Kullanici
        public ActionResult Index()
        {
            return View(db.Kullanici.ToList());
        }

        // GET: Kullanici/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici Kullanici = db.Kullanici.Find(id);
            if (Kullanici == null)
            {
                return HttpNotFound();
            }
            return View(Kullanici);
        }

        // GET: Kullanici/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,KulAdi,Sifre,TekrarSifre,Ad,SoyAd,EMail")] Kullanici Kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanici.Add(Kullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Kullanici);
        }

        // GET: Kullanici/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici Kullanici = db.Kullanici.Find(id);
            if (Kullanici == null)
            {
                return HttpNotFound();
            }
            return View(Kullanici);
        }

        // POST: Kullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,KulAdi,Sifre,TekrarSifre,Ad,SoyAd,EMail")] Kullanici Kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Kullanici);
        }

        // GET: Kullanici/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici Kullanici = db.Kullanici.Find(id);
            if (Kullanici == null)
            {
                return HttpNotFound();
            }
            return View(Kullanici);
        }

        // POST: Kullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanici Kullanici = db.Kullanici.Find(id);
            db.Kullanici.Remove(Kullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using DerinDenizProjesi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DerinDenizProjesi.Controllers
{
    public class AdminController : Controller
    {
        UyeDBEntities3 db = new UyeDBEntities3();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UrunIndex()
        {
            return View(db.Urun.ToList());
        }

        // GET: Uruns/Details/5
        public ActionResult UrunDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urun.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        // GET: Uruns/Create
        public ActionResult UrunCreate()
        {
            return View();
        }

        // POST: Uruns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UrunCreate([Bind(Include = "ID,UrunAd,UrunDetay,Resim,Kategori")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Urun.Add(urun);
                db.SaveChanges();
                return RedirectToAction("UrunIndex");
            }

            return View(urun);
        }

        // GET: Uruns/Edit/5
        public ActionResult UrunEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urun.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        // POST: Uruns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UrunEdit([Bind(Include = "ID,UrunAd,UrunDetay,Resim,Kategori")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UrunIndex");
            }
            return View(urun);
        }

        // GET: Uruns/Delete/5
        public ActionResult UrunDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urun.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        // POST: Uruns/Delete/5
        [HttpPost, ActionName("UrunDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult UrunDeleteConfirmed(int id)
        {
            Urun urun = db.Urun.Find(id);
            db.Urun.Remove(urun);
            db.SaveChanges();
            return RedirectToAction("UrunIndex");
        }

        //----------- Kullanıcılar

        public ActionResult KullaniciIndex()
        {
            return View(db.Kullanici.ToList());
        }

        // GET: Kullanicis/Details/5
        public ActionResult KullaniciDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // GET: Kullanicis/Create
        public ActionResult KullaniciCreate()
        {
            return View();
        }

        // POST: Kullanicis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KullaniciCreate([Bind(Include = "UserID,KulAdi,Sifre,TekrarSifre,Ad,SoyAd,EMail")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanici.Add(kullanici);
                db.SaveChanges();
                return RedirectToAction("KullaniciIndex");
            }

            return View(kullanici);
        }

        // GET: Kullanicis/Edit/5
        public ActionResult KullaniciEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Kullanicis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KullaniciEdit([Bind(Include = "UserID,KulAdi,Sifre,TekrarSifre,Ad,SoyAd,EMail")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("KullaniciIndex");
            }
            return View(kullanici);
        }

        // GET: Kullanicis/Delete/5
        public ActionResult KullaniciDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Kullanicis/Delete/5
        [HttpPost, ActionName("KullaniciDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult KullaniciDeleteConfirmed(int id)
        {
            Kullanici kullanici = db.Kullanici.Find(id);
            db.Kullanici.Remove(kullanici);
            db.SaveChanges();
            return RedirectToAction("KullaniciIndex");
        }
    }
}
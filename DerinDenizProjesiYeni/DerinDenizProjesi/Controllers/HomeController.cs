﻿using DerinDenizProjesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DerinDenizProjesi.Controllers
{
    public class HomeController : Controller
    {
        UyeDBEntities3 db = new UyeDBEntities3();
        public ActionResult Index()
        {
            Urunler o1 = new Urunler();
            o1.urunum = db.Urun.ToList();
            return View(o1);
        }
        public ActionResult Admin_index()
        {
           
            return View();
        }

        public ActionResult AltSayfa(int ? id)
        {
            Urun urunum = db.Urun.Find(id);
            return View(urunum);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Dive()
        {
            Urunler o1 = new Urunler();
            string a = "zipkin";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Fishing()
        {

            Urunler o1 = new Urunler();
            string a = "oltalar";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();
            return View(o1);
        }
        
        public ActionResult Kamis(Urun model)
        {
            Urunler o1 = new Urunler();
            string a = "kamislar";
            
            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper()==a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Misine()
        {
            Urunler o1 = new Urunler();
            string a = "misine";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Tulum()
        {
            Urunler o1 = new Urunler();
            string a = "tulum";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Elbise()
        {
            Urunler o1 = new Urunler();
            string a = "elbise";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Eldiven()
        {
            Urunler o1 = new Urunler();
            string a = "eldiven";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Corap()
        {
            Urunler o1 = new Urunler();
            string a = "corap";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult DalisKursun()
        {
            Urunler o1 = new Urunler();
            string a = "daliskursun";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Fener()
        {
            Urunler o1 = new Urunler();
            string a = "fener";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Palet()
        {
            Urunler o1 = new Urunler();
            string a = "palet";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Gözlük()
        {
            Urunler o1 = new Urunler();
            string a = "gözlük";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Sinorkel()
        {
            Urunler o1 = new Urunler();
            string a = "sinorkel";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();

            return View(o1);
        }
        public ActionResult Kursun()
        {
            Urunler o1 = new Urunler();
            string a = "kursun";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Yem()
        {
            Urunler o1 = new Urunler();
            string a = "yem";

            o1.urunum = db.Urun.Where(x => x.Kategori.ToUpper() == a.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult uye()
        {
            return View();
        }
        [HttpPost]
        public ActionResult uye(FormCollection form)
        {
           
            Kullanici model = new Kullanici();
            model.KulAdi = form["KulAdi"].Trim();
            model.Sifre = form["Sifre"].Trim();
            model.TekrarSifre = form["TekrarSifre"].Trim();
            model.Ad = form["Ad"].Trim();
            model.SoyAd = form["SoyAd"].Trim();
            model.EMail = form["eMail"].Trim();
            db.Kullanici.Add(model);
            db.SaveChanges();
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Kullanici model, FormCollection form)
        {
           
            
            var KULLANICI = db.Kullanici.FirstOrDefault(x => x.KulAdi == model.KulAdi && x.Sifre == model.Sifre);
            string Kullanici_Adi= form["KulAdi"].Trim();
            string sifre = form["Sifre"].Trim();
            if(Kullanici_Adi=="admin"&& sifre=="admin")
            {
                return RedirectToAction("Admin_index", "Home");
            }
            
            else if ( KULLANICI != null)
            {
                Session["KulAdi"] = KULLANICI;
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Hata = "Kullanıcı adı veya sifre yanlış!";
            return View();
        }
        public ActionResult Cikis()
        {
            Session["KulAdi"] = null;
            RedirectToAction("index", "Home");
            return View();
        }

        public class Urunler
        {
            public List<Urun> urunum { get; set; }
        }


    }
}
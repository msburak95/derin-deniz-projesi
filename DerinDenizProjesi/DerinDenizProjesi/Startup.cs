﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DerinDenizProjesi.Startup))]
namespace DerinDenizProjesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
